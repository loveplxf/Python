# -*- coding: utf-8 -*-
# 窗口
import sys
from PyQt5 import QtWidgets


class MyWidget(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.resize(400, 300)
        self.setWindowTitle("我的第一个程序")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    first_window = MyWidget()
    first_window.show()
    sys.exit(app.exec_())