# -*- coding: utf-8 -*-
# 提示信息
import sys
from PyQt5 import QtWidgets


class Tooltip(QtWidgets.QWidget):
    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.setGeometry(835, 465, 250, 150)
        self.setWindowTitle("提示信息")
        self.setToolTip("This is a <b>QWidget</b> widget")


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    tooltip = Tooltip()
    tooltip.show()
    sys.exit(app.exec_())