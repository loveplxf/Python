#-*-coding:utf8;-*-
import socket, re, random, string, time

def toupiao():
    #定义send数据
    request = '''POST /vote/web/wap/default/vote?aid=556 HTTP/1.1
Host: h5.cc.dacihua.cn
Proxy-Connection: keep-alive
Content-Length: 14
Pragma: no-cache
Cache-Control: no-cache
Accept: application/json, text/javascript, */*; q=0.01
Origin: http://h5.cc.dacihua.cn
X-Requested-With: XMLHttpRequest
X-Forwarded-For: %(ip)s
User-Agent: %(ua)s
Content-Type: application/x-www-form-urlencoded; charset=UTF-8
Referer: http://h5.cc.dacihua.cn/vote/web/wap/default/detail?aid=556&apply_id=87161
Cookie: qianfan_deviceid=%(did)s

apply_id=87161
'''
    
    #分配IP地址
    f_ip=open('ipAddr.txt', 'r')
    l_ip=f_ip.readlines()
    ipAddr = l_ip[random.randint(0, len(l_ip)-1)].strip('\n')
    print "IP地址： " + ipAddr
    
    #分配user-agent
    f_ua=open('ua.txt', 'r')
    l_ua=f_ua.readlines()
    userAgent = l_ua[random.randint(0, len(l_ua)-1)].strip('\n')    
    print "UserAgent： " + userAgent 
    
    #获得设备号
    deviceId = "a00000" + string.join(random.sample('zyxwvutsrqponmlkjihgfedcba0987654321',8)).replace(' ','')
    
    #修改request
    request = request % dict(ip = ipAddr,
                             ua = userAgent,
                             did = deviceId)
    
    #建立通道
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  #指明通信类型和协议家族
    s_port = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #测试端口
    
    #绑定本地端口
    port = random.randint(10000,65535)
    while(s_port.connect_ex(("",port)) == 0):
        port = random.randint(10000,65535)
    s.bind(("", port))
    s.connect(("h5.cc.dacihua.cn", 80))
    
    #print "Connected from", s.getsockname()  #获得本地机器的ip和端口号（端口号由操作系统随机分配，每次都不一样）
    #print "Connected to", s.getpeername()    #获得连接机器的ip和端口号
    
    s.send(request)
    rs = s.recv(1024)
    
    p_status = re.compile(r'((?<="status":).*?(?=,))')
    p_msg = re.compile(r'((?<="msg":").*?(?="))')
    m_status = p_status.findall(rs)
    m_msg = p_msg.findall(rs)
    print "投票状态：" + m_status[0]
    #print m_msg[0].decode('raw_unicode_escape')
    s.close()

sum = 0
while(True):
    sum = sum + 1
    print "第 %d 个投票 "%sum
    toupiao()
    sleepTime = random.randint(1, 60)
    print "还需再等待：%d 秒"%sleepTime
    print "==================================="
    time.sleep(sleepTime)