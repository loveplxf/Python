from PyQt4.QtGui import *  
from PyQt4.QtCore import *
from PyQt4 import uic
import sys  
import test
  
class TestDialog(QDialog,test.Ui_Dialog):  
    def __init__(self,parent=None):  
        super(TestDialog,self).__init__(parent)  
        self.setupUi(self)  
       
app=QApplication(sys.argv)
dlg=uic.loadUi("test.ui")
dlg.exec_()
app.exec_()