import wx
########################################################################
class MyFrame(wx.Frame):
    """"""

    #----------------------------------------------------------------------
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, -1, 'helloworld', size=(300, 300))
        
        panel = wx.Panel(self)
        sizer = wx.BoxSizer(wx.VERTICAL)
        panel.SetSizer(sizer)
        
        txt = wx.StaticText(panel, -1, 'helloworld!')
        
        sizer.Add(txt, 0, wx.TOP|wx.LEFT, 100)
        
        self.Centre()
        
app = wx.App()
frame = MyFrame(None)
frame.Show(True)

app.MainLoop()

    